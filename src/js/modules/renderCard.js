import visit from './class-visit.js'
import request from './request-create.js'
import {cardnothing} from './mudule_register.js'

class Create {
	constructor() {
		this.mainCard = document.querySelector('.main-cards')
		this.url = 'https://ajax.test-danit.com/api/v2/cards'
	}

	renderCard({
		doctor,
		title,
		description,
		weight,
		age,
		id,
		priority,
		full_name,
		diseases,
		mass_index,
		pressure,
		data_pressure,
		age_therapevt,
	}) {
		const card = document.createElement('div')
		card.setAttribute('data-id', id)
		card.classList.add('card')
		card.innerHTML = `
            <span class="modal-close">X</span>
        <span class="card__doctor">Doctor:${doctor}</span>
        <span class="card__name">Full name:${full_name}</span>
        <span class="card__show">Show more</span>
        <span class="card__edit">Edit</span>
        `
		this.showMore(
			doctor,
			card,
			title,
			description,
			weight,
			age,
			id,
			priority,
			full_name,
			diseases,
			mass_index,
			pressure,
			data_pressure,
			age_therapevt
		)
		this.mainCard.append(card)
		this.edit(card, id)
		const closeButton = card.querySelector('.modal-close')
		closeButton.addEventListener('click', () => {
			let token = localStorage.getItem('token')
			fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
				method: 'DELETE',
				headers: {
					Authorization: `Bearer ${token}`,
				},
			})
			card.remove()

			if (this.mainCard.childNodes.length == '1') {
				cardnothing.textContent = 'No items have been added'
			}
		})
	}
	showMore(
		doctor,
		card,
		title,
		description,
		weight,
		age,
		id,
		priority,
		full_name,
		diseases,
		mass_index,
		pressure,
		data_pressure,
		age_therapevt
	) {
		const show = card.querySelector('.card__show')
		const more = document.createElement('div')
		more.classList.add('more')
		card.append(more)
		const moreHide = card.querySelector('.card__show')
		if (doctor === '1') {
			more.innerHTML = `
            <span class="card__title">title:${title}</span>
            <span class="card__description">description:${description}</span>
            <span class="card__priority">priority:${priority}</span>
            <span class="card__pressure">pressure:${pressure}</span>
            <span class="card__mass_index">mass index:${mass_index}</span>
            <span class="card__diseases">diseases:${diseases}</span>
            <span class="card__age">age:${age}</span>
            `
		} else if (doctor === '2') {
			more.innerHTML = `
                <span class="card__title">title:${title}</span>
                <span class="card__description">description:${description}</span>
                <span class="card__priority">priority:${priority}</span>
                <span class="card__data">Last data:${data_pressure}</span>
                `
		} else if (doctor === '3') {
			more.innerHTML = `
            <span class="card__title">title:${title}</span>
            <span class="card__description">description:${description}</span>
            <span class="card__priority">priority:${priority}</span>
            <span class="card__age-therapevt">Age:${age_therapevt}</span>
            `
		}
		show.addEventListener('click', e => {
			e.preventDefault()
			moreHide.classList.toggle('moreHide')
			more.classList.toggle('visible')

			if (show.textContent === 'Show more') {
				show.textContent = 'Hide'
			} else {
				show.textContent = 'Show more'
			}
		})
	}

	updateCard({
		title,
		description,
		age,
		doctor,
		priority,
		full_name,
		id,
		diseases,
		mass_index,
		pressure,
		data_pressure,
		age_therapevt,
	}) {
		const card = this.mainCard.querySelector(`[data-id="${id}"]`)
		const titleElement = card.querySelector('.card__title')
		const descriptionElement = card.querySelector('.card__description')
		const priorityElement = card.querySelector('.card__priority')
		const doctorElement = card.querySelector('.card__doctor')
		const NameElement = card.querySelector('.card__name')
		const more = card.querySelector('.more')

		titleElement.textContent = `title:${title}`
		descriptionElement.textContent = `description:${description}`
		priorityElement.textContent = `priority:${priority}`
		doctorElement.textContent = `Doctor:${doctor}`
		NameElement.textContent = `Full name:${full_name}`

		if (doctor === '1') {
			more.innerHTML = `
            <span class="card__title">title:${title}</span>
            <span class="card__description">description:${description}</span>
            <span class="card__priority">priority:${priority}</span>
            <span class="card__pressure">pressure:${pressure}</span>
            <span class="card__mass_index">mass index:${mass_index}</span>
            <span class="card__diseases">diseases:${diseases}</span>
            <span class="card__age">age:${age}</span>
            `
		}
		if (doctor === '2') {

			more.innerHTML = `
                <span class="card__title">title:${title}</span>
                <span class="card__description">description:${description}</span>
                <span class="card__priority">priority:${priority}</span>
                <span class="card__data">Last data:${data_pressure}</span>
                `
		}
		if (doctor === '3') {

			more.innerHTML = `
        <span class="card__title">title:${title}</span>
        <span class="card__description">description:${description}</span>
        <span class="card__priority">priority:${priority}</span>
        <span class="card__age-therapevt">Age:${age_therapevt}</span>
        `
		}
	}
	edit(card, id) {
		const edit = card.querySelector('.card__edit')

		edit.addEventListener('click', e => {
			const url = this.url + '/' + id
			let token = localStorage.getItem('token')

			fetch(url, {
				method: 'GET',
				headers: {
					Authorization: `Bearer ${token}`,
				},
			})
				.then(response => response.json())
				.then(response => {
					visit.createCard(response)
				})
		})
	}
}

const create = new Create()

export default create
