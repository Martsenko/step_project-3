import nothing from './mudule_register.js'
import request from './request-create.js'
import create from './renderCard.js'
import Modal from './module_modal.js'
import {cardnothing} from './mudule_register.js'

const modal = new Modal()

class Visit {
	constructor(selectDoctor) {
		this.selectDoctor = selectDoctor
		this.container = document.querySelector('.container')
		this.urlCreateCard = 'https://ajax.test-danit.com/api/v2/cards'
	}

	createCard(response = null) {
		let modalVisible = localStorage.getItem('modalVisible')

		if (modalVisible === 'true') {
			modalVisible = false
			window.localStorage.setItem('modalVisible', modalVisible)

			modal.createForm(this.container)
			const FormCard = document.querySelector('.create-card__form')

			modal.createSelect(
				'form-content__choose',
				'doctor',
				'Choose_doctor Cardiologist Dantist Therapist',
				FormCard
			)
			const formContentChoose = document.querySelector('#doctor')
			formContentChoose.options[0].disabled = true

			modal.createInput('input', 'Purpose of visit', 'title', FormCard, true)
			modal.createInput(
				'input',
				'Descripton of the visit',
				'description',
				FormCard,
				true
			)
			modal.createSelect(
				'form-content__choose',
				'priority',
				'Ordinary Priority Urgent',
				FormCard
			)
			modal.createInput('input', 'Full name', 'full_name', FormCard, true)
			modal.createButton('Create Card', 'create_id', FormCard)
			let token = localStorage.getItem('token')
			this.selectDoctor = document.getElementById('doctor')
			if (response !== null) {
				const {title, description, doctor, priority, full_name, id} = response
				const {
					title: titleElement,
					description: descriptionElement,
					doctor: doctorElement,
					priority: priorityElement,
					full_name: full_nameElement,
				} = FormCard.elements

				titleElement.value = title
				descriptionElement.value = description
				doctorElement.value = doctor
				priorityElement.value = priority
				full_nameElement.value = full_name
				const requestType = 'put'

				this.submit(FormCard, requestType, token, id)
			} else {
				const requestType = 'add'
				this.submit(FormCard, requestType, token)
			}

			const visitCardiologist = new VisitCardiologist()
			visitCardiologist.cardiologist()
			const visitTherapist = new VisitTherapist()
			visitTherapist.therapist()
			const visitDentist = new VisitDantist()
			visitDentist.dantist()
		}
	}

	submit(FormCard, requestType, token, id) {
		FormCard.addEventListener('submit', e => {
			e.preventDefault()
			const data = {}
			const formElements = [...FormCard.elements].filter(({type}) => {
				return type !== 'submit'
			})
			formElements.forEach(({id, value}) => {
				data[id] = value
			})
			FormCard.remove()
			const requestpost = {
				data: data,
				url: `${this.urlCreateCard}`,
				token: token,
			}
			let modalVisible = localStorage.getItem('modalVisible')

			switch (requestType) {
				case 'add':
					requestpost.method = 'POST'
					request.request(requestpost).then(card => {
						create.renderCard(card)
						FormCard.remove()
						cardnothing.textContent = ''
						modalVisible = true
						window.localStorage.setItem('modalVisible', modalVisible)
					})
					break

				case 'put':
					requestpost.method = 'PUT'
					requestpost.url = `${this.urlCreateCard}/ ${id}`
					request.request(requestpost).then(card => {
						create.updateCard(card)
						FormCard.remove()
						modalVisible = true
						window.localStorage.setItem('modalVisible', modalVisible)
					})
					break
			}
		})
	}
}

const div = document.createElement('div')

div.classList.add('modal_additional-box')

class VisitCardiologist extends Visit {
	cardiologist() {
		const doctor = document.getElementById('doctor')
		const modalBtn = document.querySelector('.modal-button')
		modalBtn.before(div)

		doctor.addEventListener('change', function () {
			if (doctor.value === '1') {
				div.innerHTML = `
				        <input required class="input" type="text" id="pressure" name="pressure" placeholder="normal pressure">
				        <input required class="input" type="text" id="mass_index" name="mass-index" placeholder="body mass index">
				        <input required class="input" type="text" id="diseases" name="diseases" placeholder="transferred diseases of the cardiovascular system">
				        <input required class="input" type="text" id="age" name="age" placeholder="age">
				        `
			
			}
		})
	}
}
class VisitTherapist extends Visit {
	therapist() {
		const modalBtn = document.querySelector('.modal-button')
		modalBtn.before(div)
		doctor.addEventListener('change', () => {
			if (doctor.value === '2') {

				div.innerHTML = `
                <label class="form-content__text" for="pressure">Date of last visit</label>
                <input required class="input" type="date" id="data_pressure" name="pressure">
                `
				const pressure = document.querySelector('#pressure')
				pressure.setAttribute('max', new Date().toISOString().slice(0, -14))
			}
		})
	}
}
class VisitDantist extends Visit {
	dantist() {
		const modalBtn = document.querySelector('.modal-button')
		modalBtn.before(div)
		doctor.addEventListener('change', () => {
			if (doctor.value === '3') {

				div.innerHTML = `
				         <input required class="input" type="text" id="age_therapevt" name="age" placeholder="age">
				        `
			}
		})
	}
}

const visit = new Visit()
export default visit
