class Filter {
	searchFilter() {
		const search = document.querySelector('#formSearch')
		search.addEventListener('keyup', e => {
			const cardTitle = document.querySelectorAll('.card__title')
			const cardDescription = document.querySelectorAll('.card__description')

			cardDescription.forEach(el => {
				const cardDescText = el.innerText.split(':')[1]
				if (!cardDescText.includes(e.key)) {
					el.parentElement.parentElement.style.display = 'none'
				} else {
					el.parentElement.parentElement.style.display = 'block'
				}
				if (
					e.target.value === '' ||
					e.target.value === undefined ||
					e.target.value === ' '
				) {					el.parentElement.parentElement.style.display = 'block'
				}
				this.searchUrgency()
			})
			cardTitle.forEach(el => {
				const cardTitleText = el.innerText.split(':')[1]
				if (!cardTitleText.includes(e.key)) {
					el.parentElement.parentElement.style.display = 'none'
				} else {
					el.parentElement.parentElement.style.display = 'block'
				}
				if (
					e.target.value === '' ||
					e.target.value === undefined ||
					e.target.value === ' '
				) {
					el.parentElement.parentElement.style.display = 'block'
				}
				this.searchUrgency()
			})
		})
		const urgency = document.querySelector('#formUrgency')

		urgency.addEventListener('change', e => {
			const allCards = document.querySelectorAll('.card')
			const urgencyValue = urgency.options[urgency.selectedIndex].value

			allCards.forEach(card => {
				const more = card.querySelector('.more')
				const stringPriority = more.querySelector('.card__priority')
				let priority = stringPriority.innerText.slice(9)

				if (urgencyValue == priority) {
					card.style.display = 'block'
				} else {
					card.style.display = 'none'
				}
			})
		})

	}
	
}

export default Filter
